<?php

return [
    'default' => [
        'view' => 'adminto',
    ],

    'views' => [
        'simple' => 'menus::simple.menu',
        'bs-nav-stacked' => 'menus::bs-nav-stacked.menu',
        'sbadmin2' => 'menus::sbadmin2.menu',
        'adminto' => 'adminto::menus.sidebar.menu',
        'admin-lte' => 'menus::admin-lte.menus',
        'adminto' => 'menus::adminto.sidebar.menu',
    ],

    // 'menus' => [
    //     'sidebar' => [
    //         'view' => 'sb-nav-stacked'
    //     ]
    // ]
];
