@extends('layouts.app')

@section('title', __('User Management'))

@push('css')
  <style>
    .table > tbody > tr > td {
      vertical-align: middle;
    }
  </style>
@endpush

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="card-box">
        <div class="dropdown float-right">
          <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
            <i class="mdi mdi-dots-vertical"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <!-- item-->
            <a href="{{ route('users.create') }}" class="dropdown-item">Add a New User</a>
          </div>
        </div>

        <h4 class="header-title mt-0">{{ __('User Lists') }}</h4>

        <p class="text-muted font-13">User management and manage assigned roles.</p>

        <table id="datatable-users" class="table mb-0">
          <thead>
          <tr>
            <th style="width: 50px;">ID</th>
            <th style="width: 300px;">Name</th>
            <th style="width: 300px;">Email</th>
            <th style="width: 200px;">Role</th>
            <th style="width: 150px;">Date Created</th>
            <th>&nbsp;</th>
          </tr>
          </thead>
          <tbody>
          @foreach ($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td>
              <img src="{{ $user->avatar }}" class="rounded-circle avatar-sm mr-2" alt="{{ $user->name }} Avatar's">
              {{ $user->name }}
            </td>
            <td><a href="mailto:{{ $user->name }}<{{ $user->email }}>">{{ $user->email }}</a></td>
            <td>{{ $user->roles->pluck('name')->join(', ') ?? '-' }}</td>
            <td>{{ $user->created_at->format('Y-m-d') }}</td>
            <td>
              @canany(['update', 'changePassword', 'delete'], $user)
              <div class="dropdown float-right show">
                <a class="btn btn-primary dropdown-toggle d-none d-lg-block" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Action <i class="mdi mdi-chevron-down"></i>
                </a>

                <a href="#" class="dropdown-toggle arrow-none card-drop d-lg-none" data-toggle="dropdown" aria-expanded="false">
                  <i class="mdi mdi-dots-vertical"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                  @can('update', $user)
                    <a class="dropdown-item" href="{{ route('users.edit', $user) }}">Edit Profile</a>
                  @endcan

                  @can('changePassword', $user)
                    <a class="dropdown-item" href="{{ route('users.password', $user) }}">Change Password</a>
                  @endcan

                  @can('delete', $user)
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="{{ route('users.destroy', $user) }}" data-method="delete" data-confirm="Are you sure?">Delete</a>
                  @endcan
                </div>
              </div>
              @endcanany
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>
@endsection
