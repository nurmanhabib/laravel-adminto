<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\PermissionGroup;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'users' => [
                'users.create',
                'users.view',
                'users.update',
                'users.password',
                'users.delete',
                'users.roles.assign',
            ],

            'roles' => [
                'roles.create',
                'roles.view',
                'roles.update',
                'roles.delete',
            ],
        ];

        $roles = [
            Role::ADMINISTRATOR => $permissions, // All permissions
            Role::READONLY => [
                'users.view',
                'roles.view',
            ],
            Role::USER => []
        ];

        foreach ($permissions as $group => $abilities) {
            /** @var PermissionGroup $group */
            $group = PermissionGroup::firstOrCreate(['name' => $group]);

            foreach ($abilities as $ability) {
                /** @var Permission $perm */
                $perm = Permission::firstOrNew(['name' => $ability]);
                $perm->group()->associate($group);
                $perm->save();
            }
        }

        foreach ($roles as $name => $permissions) {
            Role::firstOrCreate(['name' => $name])->syncPermissions($permissions);
        }
    }
}
