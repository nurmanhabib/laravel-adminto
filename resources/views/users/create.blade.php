@extends('layouts.app')

@section('title', __('Create a New User'))

@section('content')
  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">
        <h4 class="header-title mt-0">Profile Information</h4>

        <p class="text-muted font-13">Complete the user account profile information and email address.</p>

        <form action="{{ route('users.store') }}" method="post">
          @csrf
          <div class="row">
            <div class="col-lg-8">
              <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" autofocus>
                @error('name')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>

              <div class="form-group">
                <label for="email" class="control-label">Email</label>
                <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                @error('email')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>

              <h4 class="header-title mt-4">Assigned Role</h4>

              <p class="text-muted font-13">Set the user role in the application.</p>

              <div class="form-group">
                <label for="role" class="control-label">Select Role</label>
                <select id="role" name="role" class="custom-select @error('role') is-invalid @enderror">
                  @foreach ($roles as $role)
                    <option value="{{ $role->id }}"
                      {{ !old('role') && $role->name == \App\Models\Role::USER ? 'selected' : '' }}
                      {{ old('role') == $role->id ? 'selected' : '' }}
                    >{{ $role->name }}</option>
                  @endforeach
                </select>
                @error('role')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>

              <h4 class="header-title mt-4">Security</h4>

              <p class="text-muted font-13">Ensure your account is using a long, random password to stay secure.</p>

              <div class="form-group">
                <label for="password" class="control-label">Password</label>
                <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror">
                @error('password')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>

              <div class="form-group">
                <label for="password_confirmation" class="control-label">Password Confirmation</label>
                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror">
                @error('password_confirmation')
                  .invalid-feedback
                @enderror
              </div>
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>
@endsection
