<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Models\Permission;
use App\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Role::class);
    }

    public function index()
    {
        $roles = Role::with('permissions')->get();

        return view('roles.index', compact('roles'));
    }

    public function create()
    {
        $abilities = Permission::all();

        return view('roles.create', compact('abilities'));
    }

    public function store(RoleRequest $request, Role $role)
    {
        $role = Role::create($request->only('name'));

        $role->syncPermissions($request->input('permissions'));

        flash()->success('Successfully created the role');

        return redirect()->route('roles.index');
    }

    public function edit(Role $role)
    {
        $role->load('permissions');

        $abilities = Permission::all();

        return view('roles.edit', compact('role', 'abilities'));
    }

    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->only('name'));

        $role->syncPermissions($request->input('permissions'));

        flash()->success('Successfully updated the role');

        return redirect()->route('roles.index');
    }

    public function destroy(Role $role)
    {
        $role->delete();

        flash()->success('Successfully deleted the role');

        return redirect()->route('roles.index');
    }
}
