<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class ApplicationSetting extends Settings
{
    public string $name;

    public string $description;

    public string $logo;

    public string $icon;

    public static function group(): string
    {
        return 'application';
    }
}
