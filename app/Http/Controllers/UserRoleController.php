<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRoleRequest;
use App\Models\Role;
use App\Models\User;

class UserRoleController
{
    public function edit(User $user)
    {
        $roles = Role::all();

        return view('users.assign-role', compact('user', 'roles'));
    }

    public function update(UserRoleRequest $request, User $user)
    {
        $user->syncRoles($request->input('role'));

        flash()->success('Successfully assigned user role');

        return redirect()->route('users.edit', $user);
    }
}
