@if ($item->hasChild())
  <li>
    <a href="javascript:void(0)">
      <span>{{ $item->getText() }}</span>
      <span class="menu-arrow"></span>
    </a>
    <ul class="nav-third-level nav" aria-expanded="false">
      @foreach ($item->getChild()->getItems() as $item)
        @include ('menus::adminto.sidebar.item2', compact('item'))
      @endforeach
    </ul>
  </li>
@else
  @if ($item->isActive())
    <li class="active">
      <a href="{{ $item->getUrl() }}">
        <i class="mdi mdi-{{ $item->getIcon() ?: 'view-dashboard' }}"></i> <span>{{ $item->getText() }}</span>
      </a>
    </li>
  @else
    <li>
      <a href="{{ $item->getUrl() }}">
        <i class="mdi mdi-{{ $item->getIcon() ?: 'view-dashboard' }}"></i> <span>{{ $item->getText() }}</span>
      </a>
    </li>
  @endif
@endif
