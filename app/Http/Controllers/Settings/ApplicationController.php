<?php

namespace App\Http\Controllers\Settings;

use App\Rules\CropperRule;
use App\Settings\ApplicationSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;

class ApplicationController
{
    public function __invoke(ApplicationSetting $setting)
    {
        return view('settings.application.index', compact('setting'));
    }

    public function update(Request $request, ApplicationSetting $setting)
    {
        $setting->name = $request->input('name');
        $setting->tagline = $request->input('name');
        $setting->save();

        flash()->success('Successfully updated application settings');

        return back();
    }

    public function updateLogo(Request $request, ApplicationSetting $setting)
    {
        $request->validate([
            'logo' => 'required|image',
            'crop' => ['required', 'json', new CropperRule],
        ]);

        $crop = json_decode($request->input('crop'), true);

        $image = Image::make($request->file('logo'));
        $image->rotate(360 - $crop['rotate']);
        $image->crop((int)$crop['width'], (int)$crop['height'], (int)$crop['x'], (int)$crop['y']);
        $image->resize(675, 110, function (Constraint $constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $image->save();

        $request->file('logo')->storePubliclyAs('logo', 'logo.png', ['disk' => 'public']);

        $setting->logo = 'storage/logo/logo.png';
        $setting->save();

        flash()->success('Successfully updated logo');

        return back();
    }

    public function updateIcon(Request $request, ApplicationSetting $setting)
    {
        $request->validate([
            'icon' => 'required|image',
            'crop' => ['required', 'json', new CropperRule],
        ]);

        $crop = json_decode($request->input('crop'), true);

        $image = Image::make($request->file('icon'));
        $image->rotate(360 - $crop['rotate']);
        $image->crop((int)$crop['width'], (int)$crop['height'], (int)$crop['x'], (int)$crop['y']);
        $image->resize(120, 120, function (Constraint $constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $image->save();

        $request->file('icon')->storePubliclyAs('icon', 'icon.png', ['disk' => 'public']);

        $setting->icon = 'storage/icon/icon.png';
        $setting->save();

        flash()->success('Successfully updated icon');

        return back();
    }
}
