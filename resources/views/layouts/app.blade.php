<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title', 'Home') - {{ $setting->name }}</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
  <meta content="Coderthemes" name="author" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- App favicon -->
  <link rel="shortcut icon" href="{{ asset($setting->icon) }}">

  <!-- App css -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />

  @stack('css')

</head>

<body>

  <div id="app">
    <!-- Begin page -->
    <div id="wrapper">

      @include('layouts.partials.topbar')

      @include('layouts.partials.left-sidebar')

    <!-- ============================================================== -->
      <!-- Start Page Content here -->
      <!-- ============================================================== -->

      <div class="content-page">
        <div class="content">

          <!-- Start Content-->
          <div class="container-fluid">

            @yield('content')

          </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('layouts.partials.footer')

      </div>

      <!-- ============================================================== -->
      <!-- End Page content -->
      <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    @include('layouts.partials.right-sidebar')

  </div>

  <!-- Vendor js -->
  <script src="{{ asset('js/app.js') }}"></script>

  @include('flash::message')

  @stack('js')

</body>
</html>
