<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('users.view');
    }

    public function view(User $user, User $model)
    {
        return $user->is($model) || $user->can('users.view');
    }

    public function create(User $user)
    {
        return $user->can('users.create');
    }

    public function update(User $user, User $model)
    {
        return $user->is($model) || $user->can('users.update');
    }

    public function delete(User $user, User $model)
    {
        if (! ($user->can('users.delete') && $user->isNot($model))) {
            return $this->deny('You cannot delete yourself');
        }

        return true;
    }

    public function changePassword(User $user, User $model)
    {
        return $user->is($model) || $user->can('users.password');
    }

    public function assignRole(User $user, User $model)
    {
        if ($user->can('users.roles.assign') && $user->is($model)) {
            return $this->deny('Unable to assign roles to yourself');
        }

        return $user->can('users.roles.assign');
    }
}
