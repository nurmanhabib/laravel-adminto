$(document).ready(function () {
  $('#datatable-users').DataTable({
    responsive: true,
    columnDefs: [
      { responsivePriority: 10001, targets: 0 },
      { responsivePriority: 1, targets: 1 },
      { responsivePriority: 2, targets: -1 },
      { orderable: false, targets: -1 },
    ]
  });
});
