@extends('layouts.app')

@section('title', __('Assign User Role'))

@section('content')
  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">
        <h4 class="header-title mt-0">Assign User Role</h4>

        <p class="text-muted font-13">Set the user role in the application.</p>

        <form action="{{ route('users.role', $user) }}" method="post">
          @csrf
          @method('PUT')
          <div class="row">
            <div class="col-8">
              <div class="form-group">
                <label for="role" class="control-label">Select Role</label>
                <select id="role" name="role" class="custom-select">
                  @foreach ($roles as $role)
                    <option value="{{ $role->id }}" {{ $user->hasRole($role) ? 'selected' : '' }}>{{ $role->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>
@endsection
