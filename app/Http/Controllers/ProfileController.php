<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController
{
    public function __invoke(Request $request)
    {
        $user = $request->user();

        return view('profile.show', compact('user'));
    }

    public function password(Request $request)
    {
        if ($request->session()->get('status') == 'password-updated') {
            session()->reflash();

            return redirect()->route('profile');
        }

        $user = $request->user();

        return view('profile.password', compact('user'));
    }
}
