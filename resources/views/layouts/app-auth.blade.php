<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title', 'Home') - {{ $setting->name }}</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
  <meta content="Coderthemes" name="author" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- App favicon -->
  <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

  <!-- App css -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />

</head>


<body class="authentication-bg">

  <div class="account-pages mt-5 mb-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
          <div class="text-center">
            <a href="{{ url('/') }}">
              <span><img src="{{ asset($setting->logo) }}" alt="" height="22"></span>
            </a>
            <p class="text-muted mt-2 mb-4">{{ $setting->description }}</p>
          </div>
          <div class="card">

            <div class="card-body p-4">

              @yield('content')

            </div> <!-- end card-body -->
          </div>
          <!-- end card -->

          @yield('footer')

        </div> <!-- end col -->
      </div>
      <!-- end row -->
    </div>
    <!-- end container -->
  </div>
  <!-- end page -->

  <!-- Vendor js -->
  <script src="{{ asset('js/app.js') }}"></script>

  @stack('js')

</body>
</html>
