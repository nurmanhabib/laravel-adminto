<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (! User::count()) {
            /** @var User $user */
            $user = User::create([
                'name' => 'Administrator',
                'email' => 'admin@laravel.app',
                'password' => bcrypt('password'),
            ]);

            $user->assignRole(Role::ADMINISTRATOR);

            /** @var User $user */
            $user = User::create([
                'name' => 'User',
                'email' => 'user@laravel.app',
                'password' => bcrypt('password'),
            ]);

            $user->assignRole(Role::USER);

            /** @var User $user */
            $user = User::create([
                'name' => 'Read Only User',
                'email' => 'readonly@laravel.app',
                'password' => bcrypt('password'),
            ]);

            $user->assignRole(Role::READONLY);

            \App\Models\User::factory(97)->create()->each(function (User $user) {
                $user->assignRole(Role::USER);
            });
        }
    }
}
