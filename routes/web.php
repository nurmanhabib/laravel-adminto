<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->home();
});

Route::group(['middleware' => ['auth', 'auth.lock']], function () {
    Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('users', App\Http\Controllers\UserController::class)->except(['show']);
    Route::get('users/{user}/change-password', [App\Http\Controllers\UserController::class, 'changePassword'])->name('users.password');
    Route::put('users/{user}/change-password', [App\Http\Controllers\UserController::class, 'updatePassword']);
    Route::get('users/{user}/role', [App\Http\Controllers\UserRoleController::class, 'edit'])->name('users.role');
    Route::put('users/{user}/role', [App\Http\Controllers\UserRoleController::class, 'update']);

    Route::get('profile', App\Http\Controllers\ProfileController::class)->name('profile');
    Route::get('change-password', [App\Http\Controllers\ProfileController::class, 'password'])->name('change-password');

    Route::resource('roles', \App\Http\Controllers\RoleController::class);

    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
        Route::get('application', App\Http\Controllers\Settings\ApplicationController::class)->name('application');
        Route::put('application', [App\Http\Controllers\Settings\ApplicationController::class, 'update'])->name('application.update');
        Route::put('application/logo', [App\Http\Controllers\Settings\ApplicationController::class, 'updateLogo'])->name('application.update.logo');
        Route::put('application/icon', [App\Http\Controllers\Settings\ApplicationController::class, 'updateIcon'])->name('application.update.icon');
    });

    Route::get('lock', [\App\Http\Controllers\Auth\LockController::class, 'lock'])->name('auth.lock');
    Route::post('unlock', [\App\Http\Controllers\Auth\LockController::class, 'unlock'])->name('auth.unlock')->withoutMiddleware('auth.lock');
    Route::get('locked', \App\Http\Controllers\Auth\LockController::class)->name('auth.locked')->withoutMiddleware('auth.lock');
});
