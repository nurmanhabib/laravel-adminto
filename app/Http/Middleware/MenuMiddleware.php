<?php

namespace App\Http\Middleware;

use App\Models\Role;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Nurmanhabib\LaravelMenu\Facades\Menu;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Menu::make('sidebar', function () use ($request) {
            Menu::link('Dashboard', route('home'));

            Menu::heading('Users');

            if (Gate::allows('viewAny', User::class)) {
                Menu::link('User Management', route('users.index'), 'account-group');
            }

            if (Gate::allows('viewAny', Role::class)) {
                Menu::link('Role Management', url('roles'), 'shield-account');
            }

            Menu::heading('Settings');

            Menu::link('Application Settings', url('settings/application'), 'cog');
        });

        return $next($request);
    }
}
