@if ($item->hasChild())
  @include ('menus::adminto.sidebar.parent', compact('item'))
@else
  @if ($item->isActive())
    <li class="active">
      <a href="{{ $item->getUrl() }}" class="waves-effect active">
        <i class="mdi mdi-{{ $item->getIcon() ?: 'view-dashboard' }}"></i> <span>{{ $item->getText() }}</span>
      </a>
    </li>
  @else
    <li>
      <a href="{{ $item->getUrl() }}">
        <i class="mdi mdi-{{ $item->getIcon() ?: 'view-dashboard' }}"></i> <span>{{ $item->getText() }}</span>
      </a>
    </li>
  @endif
@endif
