@extends('layouts.app-auth')

@section('content')
  <div class="text-center mb-4">
    <h4 class="text-uppercase mt-0">Sign In</h4>
  </div>

  <form action="{{ route('login') }}" method="post">
    @csrf
    <div class="form-group mb-3">
      <label for="emailaddress">Email address</label>
      <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" id="emailaddress" value="{{ old('email') }}" required autofocus autocomplete="email" placeholder="Enter your email">
      @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>

    <div class="form-group mb-3">
      <label for="password">Password</label>
      <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" required id="password" placeholder="Enter your password">
      @error('password')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>

    <div class="form-group mb-3">
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" name="remember" id="checkbox-signin" {{ old('remember') ? 'checked' : '' }}>
        <label class="custom-control-label" for="checkbox-signin">Remember me</label>
      </div>
    </div>

    <div class="form-group mb-0 text-center">
      <button class="btn btn-primary btn-block" type="submit">Log In</button>
    </div>

  </form>
@endsection

@section('footer')
  <div class="row mt-3">
    <div class="col-12 text-center">
      @if (Route::has('password.request'))
        <p> <a href="{{ route('password.request') }}" class="text-muted ml-1"><i class="fa fa-lock mr-1"></i>Forgot your password?</a></p>
      @endif

      @if (Route::has('register'))
        <p class="text-muted">Don't have an account? <a href="{{ route('register') }}" class="text-dark ml-1"><b>Sign Up</b></a></p>
      @endif
    </div> <!-- end col -->
  </div>
  <!-- end row -->
@endsection
