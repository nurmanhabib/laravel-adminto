<?php

namespace App\Http\Requests;

use App\Actions\Fortify\PasswordValidationRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    use PasswordValidationRules;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'name' => 'required|string|min:3|max:50',
                    'email' => ['required', 'email', Rule::unique('users')->ignore($this->route('user'))],
                ];

            case 'POST':
                return [
                    'name' => 'required|string|min:3|max:50',
                    'email' => 'required|email|unique:users',
                    'role' => 'required|exists:roles,id',
                    'password' => $this->passwordRules(),
                ];

            default:
                return [];
        }
    }
}
