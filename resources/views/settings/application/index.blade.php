@extends('layouts.app')

@section('title', 'Application Settings')

@section('content')
  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">

        <h4 class="header-title mt-0">General Settings</h4>

        <p class="text-muted font-13">{{ __('messages.descriptions.Application Settings') }}</p>

        <form action="{{ route('settings.application.update') }}" method="post">
          @csrf
          @method('PUT')
          <div class="form-group">
            <label for="name" class="control-label">Application Name</label>
            <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $setting->name) }}">
            @error('name')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>

          <div class="form-group">
            <label for="description" class="control-label">Description</label>
            <input type="text" id="description" name="description" class="form-control @error('description') is-invalid @enderror" value="{{ old('description', $setting->description) }}">
            @error('description')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>

          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">

        <h4 class="header-title mt-0">Logo Settings</h4>

        <p class="text-muted font-13">{{ __('messages.descriptions.Application Settings') }}</p>

        <img src="{{ asset($setting->logo) }}" alt="logo" class="img" style="width: 200px">

        <p class="mt-2">Recommendation Size : 135 x 22 pixels</p>

        <button type="button" class="btn btn-primary btn-change-logo">Change Logo</button>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">

        <h4 class="header-title mt-0">Icon Settings</h4>

        <p class="text-muted font-13">{{ __('messages.descriptions.Application Settings') }}</p>

        <img src="{{ asset($setting->icon) }}" alt="logo" class="img" style="width: 37px">

        <p class="mt-2">Recommendation Size : 32 x 32 pixels</p>

        <button type="button" class="btn btn-primary btn-change-icon">Change Icon</button>
      </div>
    </div>
  </div>

  <!-- Logo Settings modal -->
  <div id="logo-setting-modal" class="modal fade img-cropper" tabindex="-1" role="dialog" aria-labelledby="logo-setting-modal-label" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="logo-setting-modal-label">Change Logo</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <form action="{{ route('settings.application.update.logo') }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="file" id="input-logo" name="logo" accept="image/png" style="display: none;">
            <input type="hidden" class="crop-result" name="crop">

            <div class="if-image-exists" style="display: none;">
              <div class="img-container">
                <img class="output" alt="Cropping" src="">
              </div>

              <div class="btn-group">
                <button type="button" class="btn btn-primary btn-rotate"><i class="zmdi zmdi-rotate-right"></i> Putar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary waves-effect waves-light btn-submit">Upload and Save</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- Icon Settings modal -->
  <div id="icon-setting-modal" class="modal fade img-cropper-icon" tabindex="-1" role="dialog" aria-labelledby="logo-setting-modal-label" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="logo-setting-modal-label">Change Icon</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <form action="{{ route('settings.application.update.icon') }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="file" id="input-icon" name="icon" accept="image/png" style="display: none;">
            <input type="hidden" class="crop-result" name="crop">

            <div class="if-image-exists" style="display: none;">
              <div class="img-container" style="width: 200px; height: 200px;">
                <img class="output" alt="Cropping" src="">
              </div>

              <div class="btn-group">
                <button type="button" class="btn btn-primary btn-rotate"><i class="zmdi zmdi-rotate-right"></i> Putar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary waves-effect waves-light btn-submit">Upload and Save</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endsection

@push('css')
  <style>
    .img-container {
      margin: 10px 0;
    }

    .img-container img {
      /* This rule is very important, please don't ignore this */
      max-width: 100%;
    }

    .if-image-exists {
      display: none;
    }
  </style>
@endpush
