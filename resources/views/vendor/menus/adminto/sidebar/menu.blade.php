<ul class="metismenu" id="side-menu">
  @foreach ($menu->getItems() as $item)
    @if ($item->getType() == 'heading')
      @include ('menus::adminto.sidebar.heading', compact('item'))
    @elseif ($item->getType() == 'separator')
      @include ('menus::adminto.sidebar.heading', compact('item'))
    @else
      @include ('menus::adminto.sidebar.item', compact('item'))
    @endif
  @endforeach
</ul>
