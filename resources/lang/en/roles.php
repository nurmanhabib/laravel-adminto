<?php

return [
    'abilities-count' => '{0} Does not have the ability|{1} Only has one ability|[2,*] Has :count abilities'
];
