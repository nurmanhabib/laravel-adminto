$(function () {
  let $imgCropper = $('.img-cropper');

  if ($imgCropper) {
    let $image = $imgCropper.find('.output');
    let $cropResult = $imgCropper.find('.crop-result');

    let $inputLogo = $('#input-logo');
    let $modalLogo = $('#logo-setting-modal');
    let $btnSubmit = $imgCropper.find('.btn-submit');
    let $form = $imgCropper.find('form')

    $modalLogo.on('hidden.bs.modal', function () {
      // Get the Cropper.js instance after initialized
      let cropper = $image.data('cropper');
      cropper.destroy();
      $inputLogo.val("")
    });

    $inputLogo.on('change', function (e) {
      $modalLogo.modal({backdrop: 'static'});

      $modalLogo.on('shown.bs.modal', function () {
        $image.cropper({
          aspectRatio: 135 / 22,
          viewMode: 1,
          autoCropArea: 1,
          dragMode: 'move',
          minCropBoxWidth: 135,
          minCropBoxHeight: 22,
          crop: function (event) {
            $cropResult.val(JSON.stringify(event.detail));
          }
        });

        // Get the Cropper.js instance after initialized
        let cropper = $image.data('cropper');

        let reader = new FileReader();

        reader.onload = function () {
          let output = $image;
          output.src = reader.result;

          if (output.src) {
            cropper.replace(output.src);
            $imgCropper.find('.if-image-exists').show();
            $imgCropper.find('.if-image-empty').hide();
          }
        };

        if (e.target.files[0]) {
          reader.readAsDataURL(e.target.files[0]);
        }

        $imgCropper.find('.btn-rotate').on('click', function () {
          cropper.rotate(90);
          cropper.zoomTo(0);
        });
      });

    })
    $btnSubmit.on('click', function (e) {
      e.preventDefault();
      $form.submit();
    })

    $('.btn-change-logo').on('click', function (e) {
      e.preventDefault();
      $inputLogo.click();
    })
  }
})
