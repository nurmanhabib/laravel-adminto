<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends \Spatie\Permission\Models\Role
{
    use HasFactory;

    const ADMINISTRATOR = 'Administrator';
    const READONLY = 'Read Only';
    const USER = 'User';
}
