<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserPasswordRequest;
use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();

        return view('users.create', compact('roles'));
    }

    public function store(UserRequest $request)
    {
        /** @var User $user */
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        $user->assignRole($request->input('role'));

        flash()->success('Successfully added new users');

        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        $roles = Role::all();

        return view('users.edit', compact('user', 'roles'));
    }

    public function update(UserRequest $request, User $user)
    {
        $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ]);

        flash()->success('Successfully updated users');

        return redirect()->route('users.index');
    }

    public function changePassword(Request $request, User $user)
    {
        Gate::authorize('changePassword', $user);

        if ($request->user()->is($user)) {
            return redirect()->route('change-password');
        }

        return view('users.password', compact('user'));
    }

    public function updatePassword(UserPasswordRequest $request, User $user)
    {
        Gate::authorize('changePassword', $user);

        $user->forceFill([
            'password' => Hash::make($request->input('password'))
        ]);

        flash()->success('Password updated successfully');

        return redirect()->route('users.edit', $user);
    }

    public function destroy(User $user)
    {
        $user->delete();

        flash()->success('User deleted successfully');

        return redirect()->route('users.index');
    }
}
