@foreach (session('flash_notification', collect())->toArray() as $message)
  <script>
    @if ($message['level'] == 'danger')
      toastr["error"]("{!! $message['message'] !!}", "{{ $message['title'] }}");
    @else
      toastr["{{ $message['level'] }}"]("{!! $message['message'] !!}", "{{ $message['title'] }}");
    @endif
  </script>
@endforeach

@include('flash::status')

@include('flash::validation')

@php
  session()->forget('flash_notification')
@endphp
