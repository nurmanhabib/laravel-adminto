<?php

namespace App\Http\View\Composers;

use App\Settings\ApplicationSetting;
use Illuminate\View\View;

class ApplicationSettingComposer
{
    protected ApplicationSetting $setting;

    public function __construct(ApplicationSetting $setting)
    {
        $this->setting = $setting;
    }

    public function compose(View $view)
    {
        $view->with('setting', $this->setting);
    }
}
