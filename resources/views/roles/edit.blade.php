@extends('layouts.app')

@section('title', $role->name)

@section('content')
  <form action="{{ route('roles.update', $role) }}" method="post">
    @csrf
    @method('PUT')
    <div class="row">
      <div class="col-lg-6">
        <div class="card-box">
          <h4 class="header-title mt-0">Edit Role</h4>

          <p class="text-muted font-13">Type a role name that describes the permissions granted.</p>

          <div class="row">
            <div class="col-8">
              <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $role->name) }}">
                @error('name')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
          </div>

          <h4 class="header-title mt-3">Permissions</h4>

          <p class="text-muted font-13">Choose the abilities for this role.</p>

          @if ($abilities->isEmpty())
            <div class="alert alert-warning text-center">
              There are no capabilities available. Please define capabilities in the <code>permissions</code> table.
            </div>
          @else
            @foreach ($abilities as $ability)
              <div class="custom-control custom-switch mb-2">
                <input type="checkbox" class="custom-control-input"
                       id="ability-{{ $ability->id }}"
                       name="permissions[]"
                       value="{{ $ability->id }}"
                       {{ $role->hasPermissionTo($ability) ? 'checked' : '' }}
                >
                <label class="custom-control-label" for="ability-{{ $ability->id }}">{{ $ability->name }}</label>
              </div>
            @endforeach
          @endif

          <button type="submit" class="btn btn-primary mt-3">Save</button>
        </div>
      </div>
    </div>
  </form>
@endsection
