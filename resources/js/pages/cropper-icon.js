$(function () {
  let $imgCropper = $('.img-cropper-icon');

  if ($imgCropper) {
    let $image = $imgCropper.find('.output');
    let $cropResult = $imgCropper.find('.crop-result');

    let $inputIcon = $('#input-icon');
    let $modalIcon = $('#icon-setting-modal');
    let $btnSubmit = $imgCropper.find('.btn-submit');
    let $form = $imgCropper.find('form')

    $modalIcon.on('hidden.bs.modal', function () {
      // Get the Cropper.js instance after initialized
      let cropper = $image.data('cropper');
      cropper.destroy();
      $inputIcon.val("")
    });

    $inputIcon.on('change', function (e) {
      $modalIcon.modal({backdrop: 'static'});

      $modalIcon.on('shown.bs.modal', function () {
        $image.cropper({
          aspectRatio: 1 / 1,
          viewMode: 3,
          autoCropArea: 1,
          dragMode: 'move',
          minCropBoxWidth: 32,
          minCropBoxHeight: 32,
          crop: function (event) {
            $cropResult.val(JSON.stringify(event.detail));
          }
        });

        // Get the Cropper.js instance after initialized
        let cropper = $image.data('cropper');

        let reader = new FileReader();

        reader.onload = function () {
          let output = $image;
          output.src = reader.result;

          if (output.src) {
            cropper.replace(output.src);
            $imgCropper.find('.if-image-exists').show();
            $imgCropper.find('.if-image-empty').hide();
          }
        };

        if (e.target.files[0]) {
          reader.readAsDataURL(e.target.files[0]);
        }

        $imgCropper.find('.btn-rotate').on('click', function () {
          cropper.rotate(90);
          cropper.zoomTo(0);
        });
      });

    })
    $btnSubmit.on('click', function (e) {
      e.preventDefault();
      $form.submit();
    })

    $('.btn-change-icon').on('click', function (e) {
      e.preventDefault();
      $inputIcon.click();
    })
  }
})
