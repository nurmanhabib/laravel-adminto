@extends('layouts.app-auth')

@section('title', 'Lock Screen')

@section('content')
  <div class="text-center mb-4">
    <h4 class="text-uppercase mt-0 mb-4">Welcome Back</h4>
    <img src="{{ Auth::user()->avatar }}" width="88" alt="user-image" class="rounded-circle img-thumbnail">
    <p class="h5 mt-2">{{ Auth::user()->name }}</p>
    <p class="text-muted mb-4">Enter your password to unlock screen.</p>

  </div>

  <form action="{{ route('auth.unlock') }}" method="post">
    @csrf

    <div class="form-group mb-3">
      <label for="password">Password</label>
      <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" required="" id="password" placeholder="Enter your password">
      @error('password')
      <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>

    <div class="form-group mb-0 text-center">
      <button class="btn btn-primary btn-block" type="submit"> Unlock </button>
    </div>

  </form>
@endsection

@section('footer')
  <div class="row mt-3">
    <div class="col-12 text-center">
      <p class="text-muted">Not you? return <a href="{{ route('logout') }}" data-method="POST" class="text-dark ml-1"><b>Logout</b></a></p>
    </div> <!-- end col -->
  </div>
  <!-- end row -->
@endsection
