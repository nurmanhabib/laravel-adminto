<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LockScreenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->has('auth.locked')) {
            return redirect()->route('auth.locked');
        }

        return $next($request);
    }
}
