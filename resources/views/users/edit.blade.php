@extends('layouts.app')

@section('title', $user->name)

@section('content')
  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">
        <h4 class="header-title mt-0">Profile Information</h4>

        <p class="text-muted font-13">Update your account's profile information and email address.</p>

        <form action="{{ route('users.update', $user) }}" method="post">
          @csrf
          @method('PUT')
          <div class="row">
            <div class="col-lg-8">
              <div class="form-group">
                <label for="id" class="control-label">User ID</label>
                <input type="text" id="id" class="form-control" value="{{ $user->id }}" disabled>
              </div>

              <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $user->name) }}" autofocus>
                @error('name')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>

              <div class="form-group">
                <label for="email" class="control-label">Email</label>
                <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email', $user->email) }}">
                @error('email')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">
        <h4 class="header-title mt-0">Assigned Role</h4>

        <p class="text-muted font-13">Set the user role in the application.</p>

        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="id" class="control-label">Current Role</label>
              <input type="text" class="form-control" value="{{ $user->roles->pluck('name')->join(', ') }}" disabled>
            </div>
          </div>
        </div>

        <a href="{{ route('users.role', $user) }}" class="btn btn-primary">Change Role</a>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">
        <h4 class="header-title mt-0">Security</h4>

        <p class="text-muted font-13">Ensure your account is using a long, random password to stay secure.</p>

        <a href="{{ route('users.password', $user) }}" class="btn btn-primary">Change Password</a>
      </div>
    </div>
  </div>
@endsection
