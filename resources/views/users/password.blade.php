@extends('layouts.app')

@section('title', __('Change Password') . ' - ' . $user->name)

@section('content')
  <div class="row">
    <div class="col-lg-6">
      <div class="card-box">

        <h4 class="header-title mt-0">Change Password</h4>

        <p class="text-muted font-13">Ensure your account is using a long, random password to stay secure.</p>

        <form action="{{ route('users.password', $user) }}" method="post">
          @csrf
          @method('PUT')
          <div class="row">
            <div class="col-lg-8">
              <div class="form-group">
                <label for="password" class="control-label">Password</label>
                <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" autofocus>
                @error('password')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>

              <div class="form-group">
                <label for="password_confirmation" class="control-label">Password Confirmation</label>
                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror">
                @error('password_confirmation')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>
@endsection
