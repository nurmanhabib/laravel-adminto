<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LockController
{
    public function __invoke(Request $request)
    {
        if (!$request->session()->has('auth.locked')) {
            return redirect()->home();
        }

        return view('auth.lock');
    }

    public function lock(Request $request)
    {
        $request->session()->put('auth.locked', true);

        $request->session()->setPreviousUrl(url()->previous());

        redirect()->setIntendedUrl(url()->previous());

        return redirect()->route('auth.locked');
    }

    public function unlock(Request $request)
    {
        $request->validate(['password' => 'required|string']);

        $user = $request->user();

        if (!Hash::check($request->input('password'), $user->password)) {
            return redirect()->back()->withErrors(['password' => 'Password is invalid']);
        }

        $request->session()->forget('auth.locked');

        return redirect()->intended(route('home'));
    }
}
