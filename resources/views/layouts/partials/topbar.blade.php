<!-- Topbar Start -->
<div class="navbar-custom">
  <ul class="list-unstyled topnav-menu float-right mb-0">

    <li class="dropdown notification-list">
      <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
          <img src="{{ Auth::user()->avatar }}" alt="user-image" class="rounded-circle">
          <span class="pro-user-name ml-1">
              {{ Auth::user()->name }} <i class="mdi mdi-chevron-down"></i>
          </span>
      </a>
      <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
        <!-- item-->
        <div class="dropdown-header noti-title">
          <h6 class="text-overflow m-0">Welcome !</h6>
        </div>

        <!-- item-->
        <a href="{{ route('profile') }}" class="dropdown-item notify-item">
          <i class="fe-user"></i>
          <span>My Profile</span>
        </a>

        <!-- item-->
        <a href="javascript:void(0);" class="dropdown-item notify-item">
          <i class="fe-settings"></i>
          <span>Settings</span>
        </a>

        <!-- item-->
        <a href="{{ route('auth.lock') }}" class="dropdown-item notify-item">
          <i class="fe-lock"></i>
          <span>Lock Screen</span>
        </a>

        <div class="dropdown-divider"></div>

        <!-- item-->
        <a href="{{ route('logout') }}" data-method="POST" data-confirm="Are you sure?" data-description="You will be logged out of your account." class="dropdown-item notify-item">
          <i class="fe-log-out"></i>
          <span>Logout</span>
        </a>
      </div>
    </li>
  </ul>

  <!-- LOGO -->
  <div class="logo-box">
    <a href="{{ route('home') }}" class="logo logo-dark text-center">
      <span class="logo-lg">
        <img src="{{ asset($setting->logo) }}" alt="" height="16">
      </span>
      <span class="logo-sm">
        <img src="{{ asset('images/logo-sm.png') }}" alt="" height="24">
      </span>
    </a>
    <a href="{{ route('home') }}" class="logo logo-light text-center">
      <span class="logo-lg">
        <img src="{{ asset('images/logo-light.png') }}" alt="" height="16">
      </span>
      <span class="logo-sm">
        <img src="{{ asset('images/logo-sm.png') }}" alt="" height="24">
      </span>
    </a>
  </div>

  <ul class="list-unstyled topnav-menu topnav-menu-left mb-0">
    <li>
      <button class="button-menu-mobile disable-btn waves-effect">
        <i class="fe-menu"></i>
      </button>
    </li>

    <li>
        <h4 class="page-title-main">@yield('title', 'Home')</h4>
    </li>
  </ul>
</div>
<!-- end Topbar -->
