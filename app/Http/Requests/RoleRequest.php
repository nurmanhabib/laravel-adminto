<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => ['required', 'string', 'min:3', 'max: 20', Rule::unique('roles')],
                    'permissions.*' => [Rule::exists('permissions', 'id')]
                ];

            case 'PUT':
                return [
                    'name' => ['required', 'string', 'min:3', 'max: 20', Rule::unique('roles')->ignore($this->route('role'))],
                    'permissions.*' => [Rule::exists('permissions', 'id')]
                ];

            default:
                return [];
        }
    }
}
