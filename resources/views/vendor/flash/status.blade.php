@if (session('status'))
  <script>
    toastr["success"]("{{ __(session('status')) }}")
  </script>
@endif
