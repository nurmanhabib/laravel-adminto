@extends('layouts.app')

@section('title', __('Create a New Role'))

@section('content')
  <form action="{{ route('roles.store') }}" method="post">
    @csrf
    <div class="row">
      <div class="col-lg-6">
        <div class="card-box">

          <div class="dropdown float-right">
            <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
              <i class="mdi mdi-dots-vertical"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <!-- item-->
              <button type="submit" class="dropdown-item">Save</button>
            </div>
          </div>

          <h4 class="header-title mt-0">Role Detail</h4>

          <p class="text-muted font-13">Type a role name that describes the permissions granted.</p>

          <div class="row">
            <div class="col-lg-8">
              <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                @error('name')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
          </div>

          <h4 class="header-title mt-3">Permissions</h4>

          <p class="text-muted font-13">Choose the abilities for this role.</p>

          @if ($abilities->isEmpty())
            <div class="alert alert-warning text-center">
              There are no capabilities available. Please define capabilities in the <code>permissions</code> table.
            </div>
          @else
            @foreach ($abilities as $ability)
              <div class="custom-control custom-switch mb-2">
                <input type="checkbox" class="custom-control-input"
                       id="ability-{{ $ability->id }}"
                       name="permissions[]"
                       value="{{ $ability->id }}"
                       {{ old('permissions.' . $loop->index) == $ability->id ? 'checked' : '' }}
                >
                <label class="custom-control-label" for="ability-{{ $ability->id }}">{{ $ability->name }}</label>
              </div>
            @endforeach
          @endif
        </div>
      </div>
    </div>
  </form>
@endsection
