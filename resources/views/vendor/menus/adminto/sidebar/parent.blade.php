@if ($item->isActive())
  <li class="active">
    <a href="javascript: void(0);">
      <i class="mdi mdi-{{ $item->getIcon() ?: 'view-dashboard' }}"></i>
      <span> {{ $item->getText() }} </span>
      <span class="menu-arrow"></span>
    </a>
    <ul class="nav-second-level" aria-expanded="true">
      @foreach ($item->getChild()->getItems() as $item)
        @include ('menus::adminto.sidebar.item2', compact('item'))
      @endforeach
    </ul>
  </li>
@else
  <li>
    <a href="javascript: void(0);">
      <i class="mdi mdi-{{ $item->getIcon() ?: 'view-dashboard' }}"></i>
      <span> {{ $item->getText() }} </span>
      <span class="menu-arrow"></span>
    </a>
    <ul class="nav-second-level" aria-expanded="false">
      @foreach ($item->getChild()->getItems() as $item)
        @include ('menus::adminto.sidebar.item2', compact('item'))
      @endforeach
    </ul>
  </li>
@endif
