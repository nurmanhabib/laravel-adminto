@extends('layouts.app')

@section('title', __('Role Management'))

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="card-box">

        <div class="dropdown float-right">
          <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
            <i class="mdi mdi-dots-vertical"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <!-- item-->
            <a href="{{ route('roles.create') }}" class="dropdown-item">Create a New Role</a>
          </div>
        </div>

        <h4 class="header-title mt-0">Role Management</h4>

        <p class="text-muted font-13">List of user roles.</p>

        <p class="alert alert-info">The authority of each user can be determined the ability to access various features in the application.</p>
      </div>
    </div>
  </div>

  <div class="row">
    @foreach ($roles as $role)
      <div class="col-lg-3">
        <div class="card-box">

          @canany(['update', 'delete'], $role)
            <div class="dropdown float-right">
              <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="mdi mdi-dots-vertical"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                @can('update', $role)
                  <a href="{{ route('roles.edit', $role) }}" class="dropdown-item">Edit Permissions</a>
                @endcan

                @can('delete', $role)
                  <div class="dropdown-divider"></div>
                  <a href="{{ route('roles.destroy', $role) }}" class="dropdown-item text-danger" data-method="delete" data-confirm="Are you sure?">Delete</a>
                @endcan
              </div>
            </div>
          @endcanany

          <h4 class="header-title mt-0">{{ $role->name }}</h4>

          <p>{{ trans_choice('roles.abilities-count', $role->permissions->count(), ['count' => $role->permissions->count()]) }}</p>
        </div>
      </div>
    @endforeach
  </div>
@endsection
