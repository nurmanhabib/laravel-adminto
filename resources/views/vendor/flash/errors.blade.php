@if ($errors->isNotEmpty())
  <script>
    @if ($errors->count() == 1)
      toastr["error"]("{{ $errors->first() }}", "Validation Error");
    @else
      toastr["error"]("{{ trans_choice('validation-errors', $errors->count(), ['count' => $errors->count()]) }}", "Validation Error");
    @endif
  </script>
@endif
