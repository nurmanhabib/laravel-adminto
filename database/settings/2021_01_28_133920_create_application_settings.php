<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateApplicationSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('application.name', 'Adminto');
        $this->migrator->add('application.description', 'Application Starter');
        $this->migrator->add('application.logo', 'images/logo-dark.png');
        $this->migrator->add('application.icon', 'images/favicon.ico');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->migrator->delete('application');
    }
}
