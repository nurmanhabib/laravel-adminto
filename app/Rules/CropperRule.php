<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CropperRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $crop = json_decode($value, true);
        $cropKeys = ['x', 'y', 'width', 'height', 'rotate', 'scaleX', 'scaleY'];
        $cropInputKeys = array_keys($crop);

        if (array_diff($cropKeys, $cropInputKeys)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Crop data is invalid format.';
    }
}
